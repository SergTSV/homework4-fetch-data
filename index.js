
const url = "https://ajax.test-danit.com/api/swapi/films"
const ul = document.querySelector("ul");

async function fetchFilm() {
    const response = await fetch(url);
    const dataJson = await response.json();
        for (let element of dataJson) {
              addFilmName(element, ul);
        element.characters.forEach((characters) => {
            fillToCharacters(characters, element);
        });
    }
}

fetchFilm();

function addFilmName({name: name, episodeId: episodeId, openingCrawl: openingCrawl }, parentElement) {
    const li = document.createElement("li");
    li.dataset.id = episodeId;
    li.innerHTML = `<h2>Name of the movie: ${name}</h2><h4>Episede : ${episodeId}</h><p>Brief description: ${openingCrawl}</p><ul></ul>`;
    parentElement.append(li);
}

async function fillToCharacters(characters, data) {
    const response = await fetch(characters);
    const dataToJson = await response.json();
    console.log(data.episodeId);
    addFilmCharacters(dataToJson, data.episodeId);
}

function addFilmCharacters({ name }, id) {
    let rootElement = document.querySelector(`[data-id="${id}"]`);
    let ul = rootElement.querySelector("ul");
    let li = document.createElement("li");
    li.textContent = name;
    ul.append(li);
}

